//
//  MBMapView.h
//  TileParser
//
//  Created by Moshe Berman on 8/14/12.
//
//

#import <UIKit/UIKit.h>


#import "MBMap.h"

@class MBMapViewController;

@interface MBMapView : UIScrollView

@property (nonatomic, strong) UIView *zoomWrapper;
@property (nonatomic, strong) NSMutableArray *tileViewList;
@property (nonatomic, strong) NSMutableArray *propertyList;
@property (nonatomic, strong) NSMutableArray *colorList;
@property (assign) int tileWidth;
@property (assign) int tileHeight;

@property (nonatomic, strong) MBMapViewController *viewController;

- (id)init;
- (void)loadMap:(MBMap *)map;
//- (void)setMaxMinZoomScalesForCurrentBounds;


@end
