
//
//  MBLayerView.m
//  TileParser
//
//  Created by Moshe Berman on 8/14/12.
//
//

#import "MBLayerView.h"

#import "MBMapView.h"

//#import "UIImage+TileData.h"
#import "TileView.h"

@interface MBLayerView ()
@property (nonatomic, strong) NSDictionary *layerData;
@property (nonatomic, strong) NSArray *tilesets;
@property (nonatomic, strong) NSArray *propertyList;
@property (nonatomic, strong) MBMapView *mapView;
@end

@implementation MBLayerView

- (id)initWithLayerData:(NSDictionary *)data tilesets:(NSArray *)tilesets properyList:(NSArray *)itemList parentMap:(MBMapView *)MapView{
    
    //
    //  Read this out form the layer data. We only allow one tile size per layer,
    //  so if different tilesets have different tilesizes you're out of luck. We could
    //  try to fix this by finding the largest size, but it could adversely affect the
    //  smaller ones, so it makes no sense. Instead, grab the first tileset and use its
    //  tile dimensions.
    //
    
    _mapView = MapView;
    
    MBTileSet *workingTileset = tilesets[0];
    
    //
    //  The number of tiles comes from the layer
    //
    
    NSInteger heightInTiles = [[data objectForKey:@"height"] integerValue];
    NSInteger widthInTiles = [[data objectForKey:@"height"] integerValue];
    
    NSInteger height = heightInTiles * workingTileset.tileSize.height;
    NSInteger width = widthInTiles * workingTileset.tileSize.width;
    
    //
    //  Since we're placing these in a scroll view, the origin should be zero.
    //  We shrink the UIScrollView to fit the largest one. TMX layers
    //  are all the same size.
    //
    
    self = [super initWithFrame:CGRectMake(0, 0, width, height)];
    
    if (self) {
        
        _heightInTiles = heightInTiles;
        _widthInTiles = widthInTiles;
        
        _layerData = data;
        _tilesets = tilesets;
        _propertyList = itemList;
        
        _name = [_layerData objectForKey:@"name"];
    }
    
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void)drawMapLayer{
    if([self.name isEqualToString:@"map"]){
        return;
    }
    
    //The list of tile IDs in the layer
    NSArray *gids = [self.layerData objectForKey:@"data"];
    
    NSInteger heightInTiles = [[self.layerData objectForKey:@"height" ] integerValue];
    NSInteger widthInTiles = [[self.layerData objectForKey:@"width"] integerValue];
    
    for (NSInteger x = 0; x < widthInTiles; x++) {
        for (NSInteger y = 0; y < heightInTiles; y++) {
            
            @autoreleasepool {
                NSInteger tileIndex = (y * widthInTiles) + x;
                TileView *view = [_mapView.tileViewList objectAtIndex:tileIndex];
                
                NSInteger GID = [[gids objectAtIndex:tileIndex] integerValue];
                
                //  Skip empty tiles
                if (GID == 0) {
                    continue;
                }
                
                GID--;
                
                NSDictionary *dict = [[self propertyList] objectAtIndex:GID];
                
                if(![dict isEqual:[NSNull null]]){
                    if([self.name isEqualToString:@"number_colors"]){
                        view.tileType = kNumber;
                        NSString *colorString = [dict objectForKey:@"value"];
                        view.color = [self parseColor:colorString];
                    }else if([self.name isEqualToString:@"numbers"]){
                        view.tileType = kNumber;
                        view.Number = [[dict objectForKey:@"value"] integerValue];
                    }else if([self.name isEqualToString:@"pic_result"]){
                        view.tileType = kPic;
                        if([[dict objectForKey:@"name"] isEqualToString:@"none"]){
                            view.isNoColor = YES;
                        }else{
                            NSString *colorString = [dict objectForKey:@"value"];
                            view.color = [self parseColor:colorString];
                        }
                        
                    }
                }
            }
        }
    }
}

-(UIColor*) parseColor:(NSString*)colorString{
    NSArray* cList = [colorString componentsSeparatedByString: @"."];
    
    float red = [cList[0] floatValue]/255.;
    float green = [cList[1] floatValue]/255.;
    float blue = [cList[2] floatValue]/255.;
    return [[UIColor alloc]initWithRed:red green:green blue:blue alpha:1.0f];
}

- (UIImage *)tileAtCoordinates:(CGPoint)coordinates{
    
    NSArray *gids = [self.layerData objectForKey:@"data"];
    
    NSInteger index = (coordinates.y * [self widthInTiles]) + coordinates.x;
    
    NSInteger gid = [gids[index] integerValue];
    
    //  Ensure we don't try and check a bad GID for data,
    //  but keep in line with zero based indexing.
    
    if (gid > 0) {
        gid --;
    }
    
    //UIImage *imageAtGID = [self cache][gid];
    
    return nil;
}

@end
