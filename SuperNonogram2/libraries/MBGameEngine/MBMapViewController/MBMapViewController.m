//
//  MBMapViewController.m
//  TileParser
//
//  Created by Moshe Berman on 8/17/12.
//
//

#import "MBMapViewController.h"
#import "MBTileParser.h"
#import "MBTileSet.h"
#import "MBTileMapObject.h"
#import "HUDView.h"

@interface MBMapViewController ()

@property (nonatomic, strong) MBTileParser *parser;
@property (nonatomic, strong) MBMap *map;

@end

@implementation MBMapViewController

#pragma mark
#pragma mark - init

- (id)initWithMapName:(NSString *)name{
    self = [super init];
    if (self) {
        _totalScore = 0;
        _numOfPicViews = 0;
        _mapView = [[MBMapView alloc] init];
        _mapView.viewController = self;
        [self loadMap:name];
        
        [[self mapView] setMinimumZoomScale:1.0];
//        [[self mapView] setMaximumZoomScale:100.0];
        [[self mapView] setZoomScale:1];
        
        _isXAxis = NO;
        _isYAxis = NO;
        _shouldChoose = NO;
    }
    return self;
}

-(void)setTotalValue:(int)value{
    _totalScore = value;
    NSLog(@"_totalScore = %d",_totalScore);
    NSLog(@"_numOfPicViews = %d",_numOfPicViews);
}

#pragma mark
#pragma mark - view lifecycle

- (void)loadView{
    [super loadView];
    //[self setView:self.mapView];
}

- (void)viewDidLoad{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // map view
    [self.view addSubview:self.mapView];
    CGRect frame = [[self view]  frame];
    [[self mapView] setFrame:frame];
    self.mapView.contentSize = self.mapView.zoomWrapper.frame.size;
    //self.mapView.zoomWrapper.center = self.view.center;
    self.mapView.zoomWrapper.userInteractionEnabled = YES;
    // Calculate Min
    CGFloat xScale = self.mapView.bounds.size.width / self.mapView.zoomWrapper.frame.size.width;    // the scale needed to perfectly fit the image width-wise
    CGFloat yScale = self.mapView.bounds.size.height / self.mapView.zoomWrapper.frame.size.height;  // the scale needed to perfectly fit the image height-wise
    CGFloat minScale = MIN(xScale, yScale);
    
    minScale *= 0.9;
    
    self.mapView.minimumZoomScale = minScale;
    self.mapView. zoomScale = minScale;
    self.mapView.maximumZoomScale = minScale*5;
    
    self.mapView.zoomWrapper.center = self.view.center;
    
    // HUD
    HUDView *hud = [[[NSBundle mainBundle] loadNibNamed:@"HUDView" owner:nil options:nil] objectAtIndex:0];
    [self.view addSubview:hud];
    hud.viewController = self;
    self.mHUDRef = hud;
    [hud applyColors:self.mapView.colorList];
    
    // gesture recognizer
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    longPress.minimumPressDuration = 0.04;
    [self.mapView addGestureRecognizer:longPress];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark - inner methods



#pragma mark
#pragma mark - gestureRecognizer and touches

- (void) handleTapGesture:(UIPanGestureRecognizer *)gestureRecognizer{
    if(gestureRecognizer.numberOfTouches == 1){
         //NSLog(@"tapped");
        CGPoint touchLocation = [gestureRecognizer locationInView:self.mapView.zoomWrapper];
        
        if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
        {
            _beginTouchPos = touchLocation;
            [self checkTouch:touchLocation Begin:YES];
        }
        
        if (gestureRecognizer.state == UIGestureRecognizerStateChanged)
        {
            if((touchLocation.x > _beginTouchPos.x+15 || touchLocation.x < _beginTouchPos.x-15) && !_isYAxis){
                _isXAxis = YES;
                touchLocation.y = _beginTouchPos.y;
                [self checkTouch:touchLocation Begin:NO];
            }else if((touchLocation.y > _beginTouchPos.y+18 || touchLocation.y < _beginTouchPos.y-18) && !_isXAxis){
                _isYAxis = YES;
                touchLocation.x = _beginTouchPos.x;
                [self checkTouch:touchLocation Begin:NO];
            }
        }
        
        if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
        {
            _isYAxis = NO;
            _isXAxis = NO;
        }
    }
}

-(void) checkTouch:(CGPoint)touchPoint Begin:(BOOL)isFirstTouch{

    dispatch_async(dispatch_get_global_queue((DISPATCH_QUEUE_PRIORITY_DEFAULT), 0), ^{
        for (TileView *view in self.mapView.tileViewList) {
            if(view.tileType != kPic){
                continue;
            }
            
            if(CGRectContainsPoint(view.frame, touchPoint)){
                dispatch_async(dispatch_get_main_queue(), ^{
                    switch (_mHUDRef.ActiveHudState) {
                        case kCOLOR:
                            if(view.paintedColor != _activeColor){
                                [view applyColor:_activeColor];
                            }
                            break;
                        case kEMPTY:
                                [view applyEmpty];
                            break;
                        case kEARASER:
                                [view earaseColor];
                            break;
                            
                        default:
                            break;
                    }
                });
                
                
    //            if(isFirstTouch == YES){
    //                if(view.isChosen){
    //                    [view unChooseView:_activeColor];
    //                    _shouldChoose = NO;
    //                }else{
    //                    [view chooseView:_activeColor];
    //                    _shouldChoose = YES;
    //                }
    //            }else{
    //                if(view.isChosen && _shouldChoose == NO){
    //                    [view unChooseView:_activeColor];
    //                }else if(!view.isChosen && _shouldChoose == YES){
    //                    [view chooseView:_activeColor];
    //                }
    //            }
                break;
            }
        }
    });
}

- (BOOL)gestureRecognizer:(UIPanGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UISwipeGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

#pragma mark
#pragma mark - Loading a Game Map

- (void) loadMap:(NSString *)mapName{
    
    [self setParser: [[MBTileParser alloc] initWithMapName:mapName]];
    __weak MBMapViewController *weakSelf = self;
    
    MBTileParserCompletionBlock block = ^(MBMap *map){
        __strong MBMapViewController *strongSelf = weakSelf;
        [[strongSelf mapView] loadMap:map];
        [strongSelf setMap:map];
    };
    
    [_parser setCompletionHandler:block];
    [_parser start];
}

#pragma mark
#pragma mark - Autorotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    CGSize winSize = [[UIScreen mainScreen] bounds].size;
    CGPoint destPoint;
    if(toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight){
        destPoint = CGPointMake(winSize.height/2., winSize.width/2.);
    }else{
        destPoint = CGPointMake(winSize.width/2., winSize.height/2);
    }
    
    int value = toInterfaceOrientation;
    [UIView animateWithDuration:duration
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.mapView.zoomWrapper.center = destPoint;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Animation is Done! Current oriention is %d", value);
                     }];
}

@end;