//
//  MBMapView.m
//  TileParser
//
//  Created by Moshe Berman on 8/14/12.
//
//

#import "MBMapView.h"
#import "MBTileSet.h"
#import "MBLayerView.h"
#import "MBMapObjectGroup.h"
#import "MBMap.h"
#import "TileView.h"
#import "HUDView.h"

@interface MBMapView () <UIScrollViewDelegate>

@property (nonatomic, strong) MBMap *map;
@property (nonatomic, copy) NSString *keyForFollowedSprite;
@property (nonatomic, strong) NSMutableArray *layers;

@end

@implementation MBMapView

- (id)init{
    
    self = [super init];

    if (self) {
        _propertyList = [NSMutableArray new];
        _tileViewList = [NSMutableArray new];
        _colorList = [[NSMutableArray alloc]init];
        _layers = [@[] mutableCopy];
        _zoomWrapper = [[UIView alloc]init];
        _zoomWrapper.backgroundColor = [UIColor redColor];
        
        self.backgroundColor = [UIColor colorWithRed:222/255. green:222/255. blue:222/255. alpha:1.0f];
        self.delegate = self;
        [self setAutoresizingMask: (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth)];;
        [self setShowsHorizontalScrollIndicator:NO];
        [self setShowsVerticalScrollIndicator:NO];
        self.panGestureRecognizer.minimumNumberOfTouches = 2;
    }
    return self;
}

- (void)loadMap:(MBMap *)map{
    [self setMap:map];
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    [self parseProperties];
    [self layoutMap];
}

- (void)parseProperties{
    for (NSInteger i = 0; i < self.map.tilesets.count;i++) {
        
        MBTileSet *workingSet = [[[self map] tilesets] objectAtIndex:i];
        
        for (NSInteger j = 0; j < workingSet.mapSize.height; j++) {
            for (NSInteger k = 0; k < workingSet.mapSize.width; k++) {
                
                @autoreleasepool {
                    if ([[workingSet tileProperties] allKeys].count) {
                        
                        NSInteger index =  (j*workingSet.mapSize.width)+k;
                        
                        NSNumber *key = [NSNumber numberWithInteger:index];
                        
                        NSDictionary *tileProperties = [workingSet tileProperties];
                        
                        if(tileProperties[key] == nil){
                            NSString *string = @"nil";
                            [_propertyList addObject:string];
                        }else{
                            
                            [_propertyList addObject:tileProperties[key]];
                        }
                        
                    }
                }
            }
        }
    }
}

//
//  Layout the map in the scroll view
//

- (void)setupMapIn:(UIView*)view{
    MBTileSet *workingTileset = [self.map.tilesets objectAtIndex:0];
    CGSize tileSize = [workingTileset tileSize];
    _tileHeight = tileSize.height;
    _tileWidth = tileSize.width;
    
    NSDictionary *layerData = [self.map.layers objectAtIndex:0];
    NSInteger heightInTiles = [[layerData objectForKey:@"height" ] integerValue];
    NSInteger widthInTiles = [[layerData objectForKey:@"width"] integerValue];
    
    for (NSInteger y = 0; y < heightInTiles; y++) {
        for (NSInteger x = 0; x < widthInTiles; x++) {
            CGRect frame = CGRectMake(x*_tileWidth+x, y*_tileHeight+y, _tileWidth, _tileHeight);
            TileView *newView = [[TileView alloc]initWithFrame:frame];
            [newView setBackgroundColor:[UIColor lightGrayColor]];
            [view addSubview:newView];
            [_tileViewList addObject:newView];
        }
    }
    
    [[self zoomWrapper] setFrame: CGRectMake(0, 0, widthInTiles*_tileWidth, heightInTiles*_tileHeight)];
    [self setContentSize: [[self zoomWrapper] frame].size];
    [self addSubview:self.zoomWrapper];
}

- (void)layoutMap{
    
    [self setupMapIn:self.zoomWrapper];
    
    for (NSInteger i = 0; i < self.map.layers.count; i++) {
        //Reduces initial spike
        @autoreleasepool {
            NSDictionary *layerData = [self.map.layers objectAtIndex:i];
            [self parseLayer:layerData];
        }
    }
    
    // cleaning up
    [self.propertyList removeAllObjects];
    [self.map.layers removeAllObjects];
    self.propertyList = nil;
    self.map.layers = nil;
    self.map = nil;
}

- (void) parseLayer:(NSDictionary *)layerData{
    NSString *name = [layerData objectForKey:@"name"];
    
    if([name isEqualToString:@"map"]){
        return;
    }
    
    //The list of tile IDs in the layer
    NSArray *gids = [layerData objectForKey:@"data"];
    
    NSInteger heightInTiles = [[layerData objectForKey:@"height" ] integerValue];
    NSInteger widthInTiles = [[layerData objectForKey:@"width"] integerValue];
    
    for (NSInteger x = 0; x < widthInTiles; x++) {
        for (NSInteger y = 0; y < heightInTiles; y++) {
            
            @autoreleasepool {
                NSInteger tileIndex = (y * widthInTiles) + x;
                TileView *view = [self.tileViewList objectAtIndex:tileIndex];
                view.parentVC = _viewController;
                NSInteger GID = [[gids objectAtIndex:tileIndex] integerValue];
                
                //  Skip empty tiles
                if (GID == 0) {
                    continue;
                }
                
                GID--;
                
                NSDictionary *dict = [[self propertyList] objectAtIndex:GID];
                
                if(![dict isEqual:[NSNull null]]){
                    if([name isEqualToString:@"number_colors"]){
                        NSString *colorString = [dict objectForKey:@"value"];
                        view.color = [self parseColor:colorString];
                        [view setTileType:kNumber];
                    }else if([name isEqualToString:@"numbers"]){
                        view.NumberValue = [[dict objectForKey:@"value"] integerValue];
                        [view setTileType:kNumber];
                    }else if([name isEqualToString:@"pic_result"]){
                        _viewController.numOfPicViews++;
                        [view setTileType:kPic];
                        if([[dict objectForKey:@"name"] isEqualToString:@"none"]){
                            view.hasNoColor = YES;
                            _viewController.totalScore++;
                            view.isScored = YES;
                        }else{
                            NSString *colorString = [dict objectForKey:@"value"];
                            view.color = [self parseColor:colorString];
                            [self ColorVariety:view.color];
                            view.hasNoColor = NO;
                        } 
                    }
                }
            }
        }
    }
}

-(void)ColorVariety:(UIColor*)color{
    if(self.colorList.count == 0){
        [self.colorList addObject:color];
    }else{
        bool bAdd = YES;
        for (UIColor *colorItem in self.colorList) {
            if([color isEqual:colorItem]){
                bAdd = NO;
            }
        }
        if(bAdd){
            [self.colorList addObject:color];
        }
    }
}

-(UIColor*) parseColor:(NSString*)colorString{
    NSArray* cList = [colorString componentsSeparatedByString: @"."];
    
    float red = [cList[0] floatValue]/255.;
    float green = [cList[1] floatValue]/255.;
    float blue = [cList[2] floatValue]/255.;
    return [[UIColor alloc]initWithRed:red green:green blue:blue alpha:1.0f];
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView*)scrollView{
    return [self zoomWrapper];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        for(TileView *tileView in self.tileViewList){
            tileView.label.contentScaleFactor=scrollView.zoomScale;
        }
    });
    NSLog(@"frame = %f",self.zoomWrapper.center.x);
        NSLog(@"frame = %f",self.zoomWrapper.center.y);
    NSLog(@"frame = %f",self.center.x);
    NSLog(@"frame = %f",self.center.y);

    self.zoomWrapper.frame = [self centeredFrameForScrollView:scrollView andUIView:self.zoomWrapper];;
}

- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView {
    CGSize boundsSize = scroll.bounds.size;
    CGRect frameToCenter = rView.frame;
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else {
        frameToCenter.origin.x = 0;
    }
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else {
        frameToCenter.origin.y = 0;
    }
    
    //NSLog(@"boundsSize =%f, %f, %f, %f", scroll.bounds.origin.x, scroll.bounds.origin.y, boundsSize.width, boundsSize.height);
    //NSLog(@"frameToCenter =%f, %f, %f, %f",frameToCenter.origin.x, frameToCenter.origin.y, frameToCenter.size.width, frameToCenter.size.height);
    return frameToCenter;
}



@end
