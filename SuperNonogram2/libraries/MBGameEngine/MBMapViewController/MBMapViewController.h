//
//  MBMapViewController.h
//  TileParser
//
//  Created by Moshe Berman on 8/17/12.
//
//

#import <UIKit/UIKit.h>

#import "MBMapView.h"
#import "TileView.h"
//#import "HUDView.h"

@class HUDView;

@interface MBMapViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) HUDView *mHUDRef;
@property (nonatomic, strong) MBMapView *mapView;
@property (nonatomic, assign) CGPoint beginTouchPos;
@property (nonatomic, assign) UIColor *activeColor;
@property (nonatomic, assign) BOOL shouldChoose;
@property (nonatomic, assign) BOOL isXAxis;
@property (nonatomic, assign) BOOL isYAxis;
@property (nonatomic, assign, setter = setTotalValue:) int totalScore;
@property (nonatomic, assign) int numOfPicViews;



- (id)initWithMapName:(NSString *)name;
- (void) loadMap:(NSString *)mapName;
@end
