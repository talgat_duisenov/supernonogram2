//
//  ViewController.m
//  SuperNonogram2
//
//  Created by Talgat Duisenov on 1/5/14.
//  Copyright (c) 2014 Talgat Duisenov. All rights reserved.
//

#import "TileView.h"
#import "MBMapViewController.h"

@interface TileView ()

@end

@implementation TileView

-(TileView*)init{
    self = [super init];
    if(self){
        _tileType = kDisabled;
        _isChosen = NO;
        _isScored = NO;
    }
    return self;
}

-(void)setHasNoColor:(BOOL)hasNoColor{
    _hasNoColor = hasNoColor;
}

-(void) setTileType:(tileTypes)tileType{
    if(_tileType != tileType){
        _tileType = tileType;
    }
    
    if(_tileType == kNumber){
        if(_color && _NumberValue != 0){
            self.backgroundColor = self.color;
            
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
            label.textAlignment = NSTextAlignmentCenter;
            [self addSubview:label];
            label.text = [NSString stringWithFormat:@"%d",_NumberValue];
            label.backgroundColor = [UIColor clearColor];
            _label = label;
            
            const CGFloat* colors = CGColorGetComponents( _color.CGColor );
            float y = (0.3 * colors[0]) + (0.59 * colors[1]) + (0.11 * colors[2]);
            if(y>=0.5){
                label.textColor = [UIColor blackColor];
            }else{
                label.textColor = [UIColor whiteColor];
            }
        }
    }else if (_tileType == kPic){
        self.backgroundColor = [UIColor whiteColor];
        _paintedColor = [UIColor whiteColor];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        label.backgroundColor = [UIColor clearColor];
        _label = label;
    }
}

-(void)applyColor:(UIColor*)color{
    
    if(_hasNoColor && !_isChosen){
        _parentVC.totalScore--;
    }else if(!_hasNoColor){
        if([color isEqual:_color] && !_isScored){
            _parentVC.totalScore++;
            _isScored = YES;
        }else if(![color isEqual:_color] && _isScored){
            _parentVC.totalScore--;
            _isScored = NO;
        }
    }
    _label.text = @"";
    self.paintedColor = color;
    self.backgroundColor = color;
    self.isChosen = YES;
}

-(void)applyEmpty{
    if(_hasNoColor && _isChosen){
        _parentVC.totalScore++;
    }else if(!_hasNoColor && _isScored){
        _parentVC.totalScore--;
        _isScored = NO;
    }
    self.backgroundColor = [UIColor whiteColor];
    self.paintedColor = [UIColor whiteColor];
    self.isChosen = NO;
    _label.text = @"X";
}

-(void)earaseColor{
    if(_hasNoColor && _isChosen){
        _parentVC.totalScore++;
    }else if(!_hasNoColor && _isScored){
        _parentVC.totalScore--;
        _isScored = NO;
    }
    self.backgroundColor = [UIColor whiteColor];
    self.paintedColor = [UIColor whiteColor];
    self.isChosen = NO;
    _label.text = @"";
}

@end
