//
//  AppDelegate.h
//  SuperNonogram2
//
//  Created by Talgat Duisenov on 1/5/14.
//  Copyright (c) 2014 Talgat Duisenov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
