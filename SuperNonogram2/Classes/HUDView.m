//
//  RateView.m
//  CustomView
//
//  Created by Ray Wenderlich on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HUDView.h"

#define MenuTag 1
#define RestartTag 2
#define Color1Tag 3
#define Color2Tag 4
#define Color3Tag 5
#define Color4Tag 6
#define Color5Tag 7
#define EraserTag 8
#define UndoTag 9
#define XTag 10

@implementation HUDView

-(void)awakeFromNib{
    [super awakeFromNib];
}

- (IBAction)tapped:(UITapGestureRecognizer *)recognizer {
    CGPoint touchLocation = [recognizer locationInView:self];
    
    for (UIView *view in self.subviews) {
        if(CGRectContainsPoint(view.frame, touchLocation)){
            [self touchDetected:view];
            break;
        }
    }
}

-(void)applyColors:(NSArray*)colorList{
    for (int i=0; i<colorList.count; i++) {
        UIView *view = [self viewWithTag:i+3];
        UIColor *color = [colorList objectAtIndex:i];
        view.backgroundColor = color;
        if(i == 0){
            [self ChangeActiveColor:Color1Tag];
        }
    }
    
    for (long i=colorList.count; i<5; i++) {
        UIView *view = [self viewWithTag:i+3];
        [view removeFromSuperview];
        view = nil;
    }
}


- (void) touchDetected:(UIView*)view{
    if(view.tag == Color1Tag || view.tag == Color2Tag || view.tag == Color3Tag || view.tag == Color4Tag || view.tag == Color5Tag){
        [self ChangeActiveColor:view.tag];
        _ActiveHudState = kCOLOR;
    }else if (view.tag == MenuTag){
        
    }else if (view.tag == EraserTag){
        _ActiveHudState = kEARASER;
    }else if (view.tag == RestartTag){
        
    }else if (view.tag == UndoTag){
        
    }else if (view.tag == XTag){
        _ActiveHudState = kEMPTY;
    }
}


-(void)ChangeActiveColor:(NSInteger)index{
    UIColor *color = [self.viewController.mapView.colorList objectAtIndex:index-3];
    self.viewController.activeColor = color;
}



@end
