#import <UIKit/UIKit.h>
#import "MBMapViewController.h"

typedef enum {
    kCOLOR,
    kEMPTY,
    kEARASER
} HUD_ITEM;

@interface HUDView : UIView

@property (nonatomic, weak) MBMapViewController *viewController;
@property (nonatomic, assign) HUD_ITEM ActiveHudState;

-(void)applyColors:(NSArray*)colorList;

@end