//
//  ViewController.m
//  SuperNonogram2
//
//  Created by Talgat Duisenov on 1/5/14.
//  Copyright (c) 2014 Talgat Duisenov. All rights reserved.
//

#import "GameViewController.h"
#import "MBMapViewController.h"

@interface GameViewController ()
    @property (nonatomic, strong) MBMapViewController *mapViewController;
@end

@implementation GameViewController

#pragma mark - view cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    MBMapViewController *mapViewController = [[MBMapViewController alloc] initWithMapName:@"Dog_16x19"];
    [self presentViewController:mapViewController animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAll;
}



@end
