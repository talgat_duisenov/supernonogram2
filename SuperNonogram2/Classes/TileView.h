//
//  ViewController.h
//  SuperNonogram2
//
//  Created by Talgat Duisenov on 1/5/14.
//  Copyright (c) 2014 Talgat Duisenov. All rights reserved.
//
#import <UIKit/UIKit.h>

typedef enum {
    kNumber,
    kPic,
    kDisabled
} tileTypes;

@class MBMapViewController;

@interface TileView : UIView

@property (nonatomic, weak) MBMapViewController *parentVC;

// tile view props
@property (nonatomic, assign) tileTypes tileType;
@property (nonatomic, assign) int NumberValue;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, retain) UIColor *paintedColor;
@property (nonatomic, retain) UILabel *label;
@property (nonatomic, assign) BOOL hasNoColor;
@property (nonatomic, assign) bool isScored;

// dynamic value
@property (nonatomic, assign) BOOL isChosen;

// methods
-(void)applyColor:(UIColor*)color;
-(void)applyEmpty;
-(void)earaseColor;

@end
